const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;




var equippedSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    
    mercenaries: [{type: mongoose.Schema.Types.ObjectId, ref: 'Mercenaries'}],
    
    weapons: [{type: mongoose.Schema.Types.ObjectId, ref: 'Weapons'}],
 
    timestamps: true
});

var Equipped = mongoose.model('Equipped', equippedSchema);

module.exports = Equipped;