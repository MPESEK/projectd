const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;



var mercenarySchema = new Schema({
    name:
    {
        type: String,
        unique: true
    },
    hitPoints:   {
        type: Number,
        default: false
    },
    attackSpeed:   {
        type: Number,
        default: false
    },
    attackDamage:   {
        type: Number,
        default: false
    },
    armorClass:   {
        type: Number,
        default: false
    },
    magicResist:   {
        type: Number,
        default: false
    },
    
}, {
    timestamps: true
});

var Mercenarys = mongoose.model('Mercenary', mercenarySchema);

module.exports = Mercenarys;