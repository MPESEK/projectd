import React, { Component } from 'react';
import { Loading } from './LoadingComponent';

import { Card, CardImg, CardImgOverlay,
    CardTitle, Breadcrumb,Button  } from 'reactstrap';
import { Link } from 'react-router-dom';
import { baseUrl } from '../shared/baseUrl';
import MercStuff2 from './MercStuff2Component';
import {  postEquipped} from '../redux/ActionCreators';
import {  withRouter } from 'react-router-dom';

import { connect } from 'react-redux';


const mapDispatchToProps = dispatch => ({
   
    postEquipped: (itemId) => dispatch(postEquipped(itemId)),
  
    
  
  });
  const mapStateToProps = state => {
    return {
      weapons: state.weapons,
      gold: state.gold,
      auth: state.auth,
      mercenarys: state.mercenarys,
      equipped: state.equipped
    }
  }

    function RenderMercShopMerc ({mercenary, onClick}) {
        return (
            <Card>
                <Link to={`/MercShop/${mercenary._id}`} >
                <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                    <CardImgOverlay>
                        <CardTitle>{mercenary.name}</CardTitle>
                    </CardImgOverlay>
                </Link>
            </Card>
        );
    }

    class MercShop extends Component  {

        constructor(props) {
            super(props);
            this.state = {
                value: 0,
                displayMercs: false
            };
            this.add =  this.add.bind(this);
            
            this.getValue = this.getValue.bind(this);
            this.del = this.del.bind(this);
            
        }
        
        
        getValue(){
            return this.state.value
        }
        del = () => {
           
            this.props.toggleQuestCompleted()
            this.props.deleteAllEquippedCurrentShop()
            this.props.changeGold(this.props.equipped.equipped.gold - 10)
        } 
        add = () => {
            this.props.toggleQuestCompleted()
            var nonLegendaryArray = this.props.mercenarys.mercenarys.filter(merc => merc.isLegendary != 1);
            
            var randomArray = Array.from({length: 15}, () => Math.floor(Math.random() * 51));
            
            var myMercs = this.props.mercenarys.mercenarys
            var postMerc = this.props.postMercCurrentShop

            randomArray.map(function(name,index){
            
                
                var mercenary = nonLegendaryArray[name]
                postMerc(mercenary._id)
            });

            
            

        
        } 
        
        render(){
           

            var myMercs = this.props.mercenarys.mercenarys
            var postMerc = this.props.postMercCurrentShop
            var add = this.add
            var getValue = this.getValue
            var del = this.del
       /* if(this.props.equipped.equipped == null){
            console.log("posting equipped")
            this.props.postEquipped()
        }*/
        if (this.props.equipped.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (this.props.equipped.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{this.props.equipped.errMess}</h4>
                    </div>
                </div>
            );
        }
        if(this.props.equipped.equipped.gold <10){
            return (
                <div className="container">
                   
                    <Button href={`/quest/`}>
                        <span > Go to Quest page, you need to earn gold</span>
                    </Button>
                    
                </div>
            );

        }
        if(this.props.equipped.equipped.questCompleted){
            /*var randomArray = Array.from({length: 3}, () => Math.floor(Math.random() * 30+1));
            randomArray.map(function(name,index){
            
                
                var mercenary = myMercs[name]

                console.log("my mercs are,",myMercs)

                

                postMerc(mercenary._id)
                
                add()
                console.log("added one + ", getValue())*/
                
        

            return (
                <div style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                    <Button color="success" onClick={() => add()}>
                        <span className="fa fa-home fa-lg"> Randomize Mercenaries</span>
                    </Button>
                </div>
            );
        }
           
        
        else{
            if(this.props.equipped.equipped.gold == 10){
                return (
                    <div className="container">
                        <div className="row">
                            
                            <MercStuff2 props = {this.props} add = {this.props.add} toggleQuestCompleted= {this.props.toggleQuestCompleted} />
                        </div>
                        <div style={{
                        display: "flex",
                        marginLeft:"360px"
                      }}>
                        <Button href={`/quest`}>Too poor to refresh, take on a quest to earn more gold</Button>
                        </div>
                    </div>
                );
            }
            else{
                return (
                    <div className="container">
                        <div className="row">
                            
                            <MercStuff2 props = {this.props} add = {this.props.add} toggleQuestCompleted= {this.props.toggleQuestCompleted} />
                        </div>
                        <div style={{
                        display: "flex",
                        marginLeft:"360px"
                      }}>
                        <Button color="danger" onClick={() => del()}>
                            <span className="fa fa-credit-card"> Cost 10 Gold: Refresh Mercenaries</span>
                        </Button>
                        </div>
                    </div>
                );

            }
            

        }

        
    }
        }
        

        export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MercShop));