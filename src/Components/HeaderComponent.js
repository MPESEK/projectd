import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem, Jumbotron,
    Button, ButtonGroup, Modal, ModalHeader, ModalBody,
    Form, FormGroup, Input, Label } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { Loading } from './LoadingComponent';
class Header extends Component {
    constructor(props) {
        super(props);
    
        this.toggleNav = this.toggleNav.bind(this);
        this.state = {
            isNavOpen: false,
            isModalOpen: false,
            isModalTwoOpen: false,
            beasts: 0,
            undead: 0,
            fae: 0,
            celestial: 0,
            warrior: 0,
            mage: 0,
        
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.toggleModal2 = this.toggleModal2.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleSignup = this.handleSignup.bind(this);
        this.addBeast = this.addBeast.bind(this)
      }
      handleLogin(event) {
        this.toggleModal();
        this.props.loginUser({username: this.username.value, password: this.password.value});
        event.preventDefault();

    }
    handleSignup(event) {
        this.toggleModal2();
        

        this.props.signupUser({username: this.username.value, password: this.password.value})
        this.toggleModal();
        
        
        event.preventDefault();
        

        

      
        
      
        

    }
    handleLogout() {
        this.props.logoutUser();
    }
      toggleNav() {
        this.setState({
          isNavOpen: !this.state.isNavOpen
        });
      }

      toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      }
      toggleModal2() {
        this.setState({
          isModalTwoOpen: !this.state.isModalTwoOpen
        });
      }
      addBeast(){
        this.setState({
            beasts: this.state.beasts+1
          });
      }

    render() {
        var i = 0
        var beasts = 0
        var undead = 0
        var beastsg3 = 3
        var demon = 0
        var fae = 0
        var celestial = 0
        var nightmare = 0

        var demong3 = 3
        var undeadg3 = 3
        var celestialg3 = 3
        var faeg3 = 3
        var changeling = 0
        var spark = 0
        var ranged = 0 
        var aberration = 0
        var frost = 0
        var inferno = 0
        var nightmare = 0
        var poison = 0
        var warrior = 0
        var ethereal = 0
        var priest = 0

        var changelingg3 = 2
        var sparkg3 = 2
        var rangedg3 = 2
        var aberrationg3 = 2
        var frostg3 = 2
        var infernog3 = 2
        var nightmareg3 = 2
        var poisong3 = 2
        var warriorg3 = 2
        var etherealg3 = 2
        var priestg3 = 2


        if (this.props.equipped.isLoading) {
            
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        if(this.props.equipped.errMess == null){
            var i = 0
            var beasts = 0
            while (i < this.props.equipped.equipped.mercenarys.length){
                const roster = this.props.equipped.equipped.mercenarys[i]
                const rosterStr = roster.name.split("_")
                const origin_2 = rosterStr[2]
                var origin_3 = ""
                
                if(rosterStr.length > 2){
                    origin_3 = rosterStr[3]
                }
               
                if (roster.origin == 'beast'){
                    beasts++
                }
                if(roster.origin == 'undead'){
                    undead++;
                }
                if(roster.origin == 'fae'){
                    fae++;
                }
                if(roster.origin == 'celestial'){
                    celestial++;
                }
                if(roster.origin == 'demon'){
                    demon++;
                }
                if((origin_2 || origin_3) == 'changeling'){
                    changeling++;
                }
                if(origin_2 == 'spark'){
                    spark++;
                }
                if(origin_2 == 'ranged'){
                    ranged++;
                }
                if(origin_2 == 'aberration'){
                    aberration++;
                }
                if(origin_2 == 'frost'){
                    frost++;
                }
                if(origin_2 == 'inferno'){
                    inferno++;
                }
                if(origin_2 == 'nightmare'){
                    nightmare++;
                }
                if(origin_2 == 'poison'){
                    poison++;
                }
                if(origin_2 == 'warrior'){
                    warrior++;
                }
                if(origin_2 == 'ethereal'){
                    ethereal++;
                }
                if(origin_2 == 'priest'){
                    priest++;
                }
                i++
                
            }
            if(beasts >3 ){
                beastsg3 = 6
            }
            if(demon >3 ){
                demong3 = 6
            }
            if(undead >3 ){
                undeadg3 = 6
            }
            if(celestial >3 ){
                celestialg3 = 6
            }
            if(fae >3 ){
                faeg3 = 6
            }
            if(changeling >2 ){
                changelingg3 = 4
            }
            if(spark >2 ){
                sparkg3 = 4
            }
            if(ranged >2 ){
                rangedg3 = 4
            }
            if(aberration >2 ){
                aberrationg3 = 4
            }
            if(frost >2 ){
                frostg3 = 4
            }
            if(inferno >3 ){
                infernog3 = 4
            }
            if(nightmare >2 ){
                nightmareg3 = 4
            }
            if(poison >2 ){
                poisong3 = 4
            }
            if(warrior >2 ){
                warriorg3 = 4
            }
            if(ethereal >2 ){
                etherealg3 = 4
            }
            if(priest >2 ){
                priestg3 = 4
            }
            
            
            return <div>
            <Navbar dark expand="md">
                <div className="container">
                    <NavbarToggler onClick={this.toggleNav} />
                    <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='Project D' /></NavbarBrand>
                    <Collapse isOpen={this.state.isNavOpen} navbar>
                        <Nav navbar>
                        
                        <NavItem>
                            <NavLink className="nav-link"  to='/mercShop'><span className="fa fa-home fa-lg"></span> Merc Shop</NavLink>
                            
                        </NavItem>
                        <NavItem>
                            <NavLink className="nav-link"  to='/roster'><span className="fa fa-user fa-lg"></span> Roster</NavLink>
                            
                        </NavItem>
                        <NavItem>
                        
                            <NavLink className="nav-link"  to='/quest'><span className="fa fa-shield fa-lg"></span> Quests</NavLink>
                        </NavItem>
                        </Nav>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                { !this.props.auth.isAuthenticated ?
                                    <ButtonGroup>
                                    <Button outline onClick={this.toggleModal}>
                                        <span className="fa fa-sign-in fa-lg"></span> Login
                                        {this.props.auth.isFetching ?
                                            <span className="fa fa-spinner fa-pulse fa-fw"></span>
                                            : null
                                        }
                                    </Button>
                                    <Button outline onClick={this.toggleModal2}>
                                    <span className="fa fa-sign-in fa-lg"></span> Signup
                                    {this.props.auth.isFetching ?
                                        <span className="fa fa-spinner fa-pulse fa-fw"></span>
                                        : null
                                    }
                                    </Button>
                                    </ButtonGroup>
                                    :
                                    <div>
                                    <div className="navbar-text mr-3">{this.props.auth.user.username}</div>
                                    <Button outline onClick={this.handleLogout}>
                                        <span className="fa fa-sign-out fa-lg"></span> Logout
                                        {this.props.auth.isFetching ?
                                            <span className="fa fa-spinner fa-pulse fa-fw"></span>
                                            : null
                                        }
                                    </Button>
                                    </div>
                                }

                            </NavItem>
                        </Nav>
                    </Collapse>
                </div>
            </Navbar>
            <Jumbotron>
                <div className="container">
                    <div className="row row-header">
                        
                        <div className="col-2 col-sm-12">
                        <span className="fa fa-credit-card fa-lg"> <h1 style={{ color: 'gold' }}>{this.props.equipped.equipped.gold}</h1></span>

                           
                            
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Beast ({beasts}/{beastsg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Demons ({demon}/{demong3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Celestial ({celestial}/{celestialg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>     ---Fae---     ({fae}/{faeg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Undead ({undead}/{undeadg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Changeling ({changeling}/{changelingg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                           
                            <p>Sparks ({spark}/{sparkg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Ranged ({ranged}/{rangedg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Aberration ({aberration}/{aberrationg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            
                            <p>Frost ({frost}/{frostg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                           
                            <p>Inferno ({inferno}/{infernog3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            <p>Nightmare ({nightmare}/{nightmareg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            <p>Poison ({poison}/{poisong3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            <p>Warrior ({warrior}/{warriorg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            <p>Ethereal ({ethereal}/{etherealg3}) </p>                              
                        </div>
                        <div className="col-2 col-sm-1">
                            <p>Priest ({priest}/{priestg3}) </p>                              
                        </div>
                    </div>
                </div>
            </Jumbotron>
            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                <ModalBody>
                <Form onSubmit={this.handleLogin}>
                        <FormGroup>
                            <Label htmlFor="username">Username</Label>
                            <Input type="text" id="username" name="username"
                                innerRef={(input) => this.username = input} />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="password">Password</Label>
                            <Input type="password" id="password" name="password"
                                innerRef={(input) => this.password = input}  />
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                                <Input type="checkbox" name="remember"
                                innerRef={(input) => this.remember = input}  />
                                Remember me
                            </Label>
                        </FormGroup>
                        <Button type="submit" value="submit" color="primary">Login</Button>
                    </Form>
                </ModalBody>
            </Modal>
            <Modal isOpen={this.state.isModalTwoOpen} toggle={this.toggleModal2}>
                    <ModalHeader toggle={this.toggleModal2}>Signup</ModalHeader>
                    <ModalBody>
                    <Form onSubmit={this.handleSignup}>
                            <FormGroup>
                                <Label htmlFor="username">Username</Label>
                                <Input type="text" id="username" name="username"
                                    innerRef={(input) => this.username = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input}  />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="remember"
                                    innerRef={(input) => this.remember = input}  />
                                    Remember me
                                </Label>
                            </FormGroup>
                            <Button type="submit" value="submit" color="primary">Login</Button>
                        </Form>
                    </ModalBody>
                </Modal>
        </div>
        }
        var check = this.props.equipped.errMsg=="Error 401: Unauthorized"
        
        var i = 0
        var beasts = 0
        
        return(
            <div>
                <Navbar dark expand="md">
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='Project D' /></NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                            
                            <NavItem>
                                <NavLink className="nav-link"  to='/mercShop'><span className="fa fa-home fa-lg"></span> Merc Shop</NavLink>
                                
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link"  to='/roster'><span className="fa fa-user fa-lg"></span> Roster</NavLink>
                                
                            </NavItem>
                            <NavItem>
                            
                                <NavLink className="nav-link"  to='/quest'><span className="fa fa-shield fa-lg"></span> Quests</NavLink>
                            </NavItem>
                            </Nav>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    { !this.props.auth.isAuthenticated ?
                                        <ButtonGroup>
                                        <Button outline onClick={this.toggleModal}>
                                            <span className="fa fa-sign-in fa-lg"></span> Login
                                            {this.props.auth.isFetching ?
                                                <span className="fa fa-spinner fa-pulse fa-fw"></span>
                                                : null
                                            }
                                        </Button>
                                        <Button outline onClick={this.toggleModal2}>
                                        <span className="fa fa-sign-in fa-lg"></span> Signup
                                        {this.props.auth.isFetching ?
                                            <span className="fa fa-spinner fa-pulse fa-fw"></span>
                                            : null
                                        }
                                        </Button>
                                        </ButtonGroup>
                                        :
                                        <div>
                                        <div className="navbar-text mr-3">{this.props.auth.user.username}</div>
                                        <Button outline onClick={this.handleLogout}>
                                            <span className="fa fa-sign-out fa-lg"></span> Logout
                                            {this.props.auth.isFetching ?
                                                <span className="fa fa-spinner fa-pulse fa-fw"></span>
                                                : null
                                            }
                                        </Button>
                                        </div>
                                    }

                                </NavItem>
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
                <Jumbotron>
                    <div className="container">
                        <div className="row row-header">
                            <div className="col-2 col-sm-12">
                                
                                <p>Please Signup or Login to enter a game. All it takes is a username and a password</p>
                            </div>
                        </div>
                    </div>
                </Jumbotron>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                    <ModalBody>
                    <Form onSubmit={this.handleLogin}>
                            <FormGroup>
                                <Label htmlFor="username">Username</Label>
                                <Input type="text" id="username" name="username"
                                    innerRef={(input) => this.username = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input}  />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="remember"
                                    innerRef={(input) => this.remember = input}  />
                                    Remember me
                                </Label>
                            </FormGroup>
                            <Button type="submit" value="submit" color="primary">Login</Button>
                        </Form>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.isModalTwoOpen} toggle={this.toggleModal2}>
                    <ModalHeader toggle={this.toggleModal2}>Signup</ModalHeader>
                    <ModalBody>
                    <Form onSubmit={this.handleSignup}>
                            <FormGroup>
                                <Label htmlFor="username">Username</Label>
                                <Input type="text" id="username" name="username"
                                    innerRef={(input) => this.username = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input}  />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="remember"
                                    innerRef={(input) => this.remember = input}  />
                                    Remember me
                                </Label>
                            </FormGroup>
                            <Button type="submit" value="submit" color="primary">Signup</Button>
                        </Form>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}
export default Header;
