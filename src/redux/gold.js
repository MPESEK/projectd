import * as ActionTypes from './ActionTypes';

export const Gold = (state = {
        gold: [120]
    }, action) => {
    switch(action.type) {
        case ActionTypes.UPDATE_GOLD:
            return {...state,errMess: true, gold: state.gold.concat(action.payload)};

    
        case ActionTypes.LAST_GOLD:
            var length = state.gold.length
            return {...state, isLoading: true, errMess: null, gold: state.gold[length-1]};


        default:
            return state;
    }
}