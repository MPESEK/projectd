
import * as ActionTypes from './ActionTypes';
export const Mercenarys = (state = { isLoading: true,
    errMess: null,
    mercenarys:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_MERCENARYS:
            return {...state, isLoading: false, errMess: null, mercenarys: action.payload};

        case ActionTypes.ADD_MERCENARYS:
            return {...state, isLoading: true, errMess: null, mercenarys: []}

        case ActionTypes.ADD_MERCENARYS:
            return {...state, isLoading: false, errMess: action.payload, mercenarys: []};

        default:
            return state;
    }
};