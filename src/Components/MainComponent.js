import React, { Component } from 'react';
import Home from './HomeComponent';
import ItemShop from './ItemShopComponent';
import MercShop from './mercShopComponent';
import Stats from './StatsComponent';
import Header from './HeaderComponent';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { fetchWeapons} from '../redux/ActionCreators';
import { fetchEquipped, postEquipped, deleteEquipped, loginUser,signupUser,snl, logoutUser,updateGold,changeGold ,postMercEquipped,fetchMercenarys,
          postMercBench,deleteEquippedBench,deleteEquippedMerc,postMercCurrentShop,deleteEquippedCurrentShop,deleteAllEquippedCurrentShop,toggleQuestCompleted} from '../redux/ActionCreators';

import ItemDetail from './itemDetailComponent';
import MercDetail from './mercDetailComponent';
import QuestDetail from './QuestDetailComponent';
import Quest from './QuestComponent';
import Roster from './Roster';


import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  fetchWeapons: () => { dispatch(fetchWeapons())},
  fetchEquipped: () => dispatch(fetchEquipped()),
  fetchMercenarys: () => dispatch(fetchMercenarys()),
  postEquipped: (itemId) => dispatch(postEquipped(itemId)),

  deleteEquipped: (itemId) => dispatch(deleteEquipped(itemId)),
  deleteEquippedMerc: (mercId) => dispatch(deleteEquippedMerc(mercId)),
  deleteEquippedBench: (mercId) => dispatch(deleteEquippedBench(mercId)),
  deleteEquippedCurrentShop: (mercId) => dispatch(deleteEquippedCurrentShop(mercId)),
  deleteAllEquippedCurrentShop: () => dispatch(deleteAllEquippedCurrentShop()),
  loginUser: (creds) => dispatch(loginUser(creds)),
  signupUser: (creds) => dispatch(signupUser(creds)),
  snl: (creds) => dispatch(snl(creds)),
  logoutUser: () => dispatch(logoutUser()),
  toggleQuestCompleted: () => {dispatch(toggleQuestCompleted())},
  updateGold: (amount) => {dispatch(updateGold(amount))},
  changeGold: (goldAmount) => dispatch(changeGold(goldAmount)),
  postMercEquipped: (mercId) => dispatch(postMercEquipped(mercId)),
  postMercBench: (mercId) => dispatch(postMercBench(mercId)),
  postMercCurrentShop: (mercId) => dispatch(postMercCurrentShop(mercId))
  

});
const mapStateToProps = state => {
  return {
    weapons: state.weapons,
    gold: state.gold,
    auth: state.auth,
    mercenarys: state.mercenarys,
    equipped: state.equipped
  }
}


class Main extends Component {


  constructor(props) {
    super(props);

    
    
    
  }
  
  
  componentDidMount() {
    this.props.fetchEquipped()
    this.props.fetchMercenarys()
  }
 
  

  
render() {
  const itemWithId = ({match}) => {
    
    
    return(
      this.props.auth.isAuthenticated
      ?
      
      <ItemDetail weapon={this.props.weapons.weapons.filter((weapon) => weapon._id === match.params.weaponId)[0]}
        isLoading={this.props.weapons.isLoading}
        errMess={this.props.weapons.errMess}
   
        equipped={this.props.equipped.equipped.weapons.some((weapon) => weapon._id === match.params.weaponId)}
        postEquipped={this.props.postEquipped}
        gold={this.props.gold}
        updateGold={this.props.updateGold}
        changeGold={this.props.changeGold}
        />
      :
      <ItemDetail weapon={this.props.weapons.weapons.filter((weapon) => weapon._id === match.params.weaponId)[0]}
        isLoading={this.props.weapons.isLoading}
        errMess={this.props.weapons.errMess}
  
        equipped={false}
        postequipped={this.props.postequipped}
        />
    );
  }

  const mercWithId = ({match}) => {
    
    return(
      this.props.auth.isAuthenticated
      ?
      
      <MercDetail mercenary={this.props.mercenarys.mercenarys.filter((mercenary) => mercenary._id === match.params.mercenaryId)[0]}
        isLoading={this.props.mercenarys.isLoading}
        errMess={this.props.mercenarys.errMess}
   
        equipped={this.props.equipped}
        roster = {this.props.equipped.equipped.mercenarys}
        postMercEquipped={this.props.postMercEquipped}
        postMercBench={this.props.postMercBench}
        deleteEquippedMerc={this.props.deleteEquippedMerc}
        deleteEquippedBench={this.props.deleteEquippedBench}
        deleteEquippedCurrentShop={this.props.deleteEquippedCurrentShop}
        toggleQuestCompleted={this.props.toggleQuestCompleted}
        gold={this.props.equipped.equipped.gold}
        updateGold={this.props.updateGold}
        changeGold={this.props.changeGold}
        />
      :
      <MercDetail mercenary={this.props.mercenarys.mercenarys.filter((mercenary) => mercenary._id === match.params.mercenaryId)[0]}
        isLoading={this.props.mercenarys.isLoading}
        errMess={this.props.mercenarys.errMess}
  
        equipped={this.props.equipped}
        postMercequipped={this.props.postMercEquipped}
        postMercBench={this.props.postMercBench}
        deleteEquippedCurrentShop={this.props.deleteEquippedCurrentShop}
        />
    );
  }

  const questWithId = ({match}) => {
  
    
    
    return(
      this.props.auth.isAuthenticated
      ?
      
      <QuestDetail 
        number = {match.params.questId}
        isLoading={this.props.mercenarys.isLoading}
        errMess={this.props.mercenarys.errMess}
        equipped = {this.props.equipped}
        updateGold = {this.props.updateGold}
        changeGold={this.props.changeGold}
        postMercEquipped={this.props.postMercEquipped}
        postMercBench={this.props.postMercBench}
        gold={this.props.gold}
        
        />
      :
      <QuestDetail 
        isLoading={this.props.mercenarys.isLoading}
        errMess={this.props.mercenarys.errMess}
        postMercEquipped={this.props.postMercEquipped}
        postMercBench={this.props.postMercBench}
        gold={this.props.gold}
        updateGold={this.props.updateGold}
        changeGold={this.props.changeGold}
        />
    );
  }

  const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      this.props.auth.isAuthenticated
        ? <Component {...props} />
        : <Redirect to={{
            pathname: '/home',
            state: { from: props.location }
          }} />
    )} />
  );


return (
    
    <div>
      <Header auth={this.props.auth} 
          loginUser={this.props.loginUser} 
          logoutUser={this.props.logoutUser} 
          signupUser={this.props.signupUser}
          snl={this.props.snl}
          gold={this.props.gold}
          equipped = {this.props.equipped}
          postEquipped = {this.props.postEquipped}
          />   
      <div>
        <TransitionGroup>
            <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
              <Switch location={this.props.location}>
                  <Route path='/home' component={Home} />
                  <PrivateRoute exact path='/roster' component={() => <Roster postMercBench={this.props.postMercBench} postMercEquipped ={this.props.postMercEquipped}
                                                                equipped={this.props.equipped} deleteEquippedMerc ={this.props.deleteEquippedMerc}
                                                          
                                                                deleteEquippedBench = {this.props.deleteEquippedBench}/>} />
                  <PrivateRoute exact path='/itemShop' component={() => <ItemShop weapons={this.props.weapons} />} />
                  <PrivateRoute exact path='/mercShop' component={() => <MercShop postMercCurrentShop={this.props.postMercCurrentShop} 
                                                                          postMercBench={this.props.postMercBench} equipped = {this.props.equipped} 
                                                                          toggleQuestCompleted={this.props.toggleQuestCompleted} mercenarys={this.props.mercenarys}
                                                                          changeGold = {this.props.changeGold} postEquipped={this.props.postEquipped}
                                                                          deleteAllEquippedCurrentShop={this.props.deleteAllEquippedCurrentShop} />} 
                                                                          />
                  <PrivateRoute path="/mercShop/:mercenaryId" component={mercWithId} />
                  <PrivateRoute path="/questDetail/:questId" component={questWithId} />
                  <PrivateRoute exact path='/stats' component={() => <Stats equipped={this.props.equipped} />} />
                  <PrivateRoute exact path='/quest' component={() => <Quest equipped={this.props.equipped} />} />
                  <Redirect to="/home" />
              </Switch>
            </CSSTransition>
          </TransitionGroup>
      </div>
     
    </div>
  );
}
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));