import React, { useState } from 'react';
import { Collapse, Button, CardBody, Card,CardImg } from 'reactstrap';
import { baseUrl } from '../shared/baseUrl';



function Home(props) {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    const [isOpen2, setIsOpen2] = useState(false);

    const toggle2 = () => setIsOpen2(!isOpen2);

    const [isOpen3, setIsOpen3] = useState(false);

    const toggle3 = () => setIsOpen3(!isOpen3);

    const [isOpen4, setIsOpen4] = useState(false);

    const toggle4 = () => setIsOpen4(!isOpen4);
    
    
    return (
        <div>
          <Button color="warning" onClick={toggle4} style={{ width: '100%',marginBottom: '1rem' }}>What is that black jumbotron above me?</Button>
          <Collapse isOpen={isOpen4}>
          <div className="col-12 col-md-6 m-1 mx-auto"> 
            <Card>
              <CardBody>
              The goal of this game is to create a team that synergises well together. All the synergies, along with your gold are displayed above. If you purchase 3 beasts, your 0/3 beast score will turn into 3/6.
              There are two tiers for each synergy. That is, if you see a 0/2 or 0/3, this means you get a bonus once you get 2 and 3 of each respectively, as well as another bonus for 4 and 6 respectively.     </CardBody>
            </Card>
            </div>
          </Collapse>  
            
          <Button color="muted" onClick={toggle} style={{ width: '100%',marginBottom: '1rem' }}>How to Use the Mercenary Shop</Button>
          <Collapse isOpen={isOpen}>
            <div className="col-12 col-md-6 m-1 mx-auto"> 
              <Card >
                <CardBody>
                This page allows you to spend gold to view a random assortment of mercenaries for hire, unless it is your first time, then they are free. Click a card to view your favorites and have the option to purchase them for 10 gold. HINT: You are rewarded for purchasing the same merc multiple times. More info on this is in the roster tab. 
                If you are unhappy with your random mercenaries don't buy any! Just spend 10 gold to refresh your options with the red button at the bottom.
                </CardBody>
              </Card>
              <Card top width ="50%">
                  <CardImg top width="100%" src={baseUrl + "images/9shop.PNG"} alt="hello"/>
                  
              </Card>
            </div>
            
          </Collapse>

          <Button color="warning" onClick={toggle2} style={{ width: '100%',marginBottom: '1rem' }}>How to Use the Roster</Button>
          <Collapse isOpen={isOpen2}>
          <div className="col-12 col-md-6 m-1 mx-auto"> 
            <Card>
              <CardBody>
                  Purchased mercenaries are moved to your roster. You can hold only 5 at a time though, so the rest are held on your bench. Move between the two using the buttons below the mercenaries. This is further explained on the Roster page.              </CardBody>
            </Card>
            <Card>
                <CardImg top width="10%" src={baseUrl + "images/roster.PNG"} alt="hello"/>
                
            </Card>
            </div>
          </Collapse>

          <Button color="muted" onClick={toggle3} style={{ width: '100%',marginBottom: '1rem' }}>How to Quest!</Button>
          <Collapse isOpen={isOpen3}>
          <div className="col-12 col-md-6 m-1 mx-auto"> 
            <Card>
              <CardBody>
               Once you have a roster you are happy with, try a quest! Succeeding will fill your pride and gold coffers. If you fail, you still get gold and can purchase new mercenaries to strengthen your team for the next quest.    </CardBody>
            </Card>
            </div>
          </Collapse>
          
        </div>
      );
    }

export default Home;

