import * as ActionTypes from './ActionTypes';

export const Equipped = (state = {
        isLoading: true,
        errMess: null,
        equipped: null
    }, action) => {
    switch(action.type) {
        case ActionTypes.ADD_EQUIPPED:
            return {...state, isLoading: false, errMess: null, equipped: action.payload};

        case ActionTypes.EQUIPPED_LOADING:
            return {...state, isLoading: true, errMess: null, equipped: null};

        case ActionTypes.EQUIPPED_FAILED:
            return {...state, isLoading: false, errMess: action.payload, equipped: null};

        default:
            return state;
    }
}