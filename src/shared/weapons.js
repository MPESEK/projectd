export const ITEMS =
    [
        {
            name: "falchion",
            attackSpeed: 1.0,
            attackDamge: 5.0,
            image: '/assets/images/bladeItself.png',
        },
        {
            name: "dagger",
            attackSpeed: 2.0,
            attackDamge: 2.5,
            image: '/assets/images/dagger.png',
        },{
            name: "The Blade itself",
            attackSpeed: 1.0,
            attackDamge: 6.0,
            image: '/assets/images/treasure.png',
        },
    ];