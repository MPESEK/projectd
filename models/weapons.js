const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;



var weaponSchema = new Schema({
    name:
    {
        type: String,
        unique: true
    },
    attackSpeed:   {
        type: Number,
        default: false
    },
    attackDamage:   {
        type: Number,
        default: false
    },
    
}, {
    timestamps: true
});

var Weapons = mongoose.model('Weapon', weaponSchema);

module.exports = Weapons;