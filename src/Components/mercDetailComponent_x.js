import React from 'react';
import { Card, CardImg, CardImgOverlay, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';


    function RenderItem({roster,mercenary, equipped, postMercEquipped,postMercBench,gold,updateGold,deleteEquippedCurrentShop}) {
            
            
            const wrapperFunction = (equipped,id,gold) => {

                var length  = gold.gold.length
                var newGold = gold.gold[length-1]
                
                var i = 0
                var rosterCount = 0
                var benchCount = 0
                var currentShopCount = 0
                while(i < equipped.equipped.mercenarys.length){
                    if (equipped.equipped.mercenarys[i]._id == id){
                        rosterCount++
                    }
                    i++

                }
                var j = 0
                while(j < equipped.equipped.bench.length){
                    if (equipped.equipped.bench[j]._id == id){
                        benchCount++
                    }
                    j++

                    
                }
                var k = 0
                while(k < equipped.equipped.currentShop.length){
                    if (equipped.equipped.currentShop[k]._id == id){
                        currentShopCount++
                    }
                    k++

                    
                }
                if(currentShopCount == 2){
                    if(roster.length <4){
                        postMercEquipped(id)
                        postMercEquipped(id)
                        deleteEquippedCurrentShop(id)
                    }
                    else if(roster.length == 4){
                        postMercEquipped(id)
                        postMercBench(id)
                        deleteEquippedCurrentShop(id)
                    }
                    else{
                        postMercBench(id)
                        postMercBench(id)
                        deleteEquippedCurrentShop(id)
                    }
                    return
                }
        
                if((rosterCount+benchCount) >= 3){
                    deleteMercEquipped(id)
                    deleteMercBench(id)
                    postMercEquipped(id.legendaryId)
                    deleteEquippedCurrentShop(id);
                    return
                    
                }
                

                if (roster.length < 5) {
                    postMercEquipped(id);
                    deleteEquippedCurrentShop(id);
                }
                else{
                    postMercBench(id);
                    deleteEquippedCurrentShop(id);
                }
                
            } // end of wrapper function
            if (gold.gold < 0){
                return <div>Go do some quests</div>
            }
            else{
            return(
                <div className="col-12 col-md-5 m-1">
                    <p>Hello</p>
                        <Card>
                            <CardImg top src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                            <CardImgOverlay>
                                <Button outline color="primary" onClick={() => wrapperFunction(equipped,mercenary._id,gold)}>
                                    {equipped ?
                                        <span className="fa fa-heart"></span>
                                        : 
                                        <span className="fa fa-heart-o"></span>
                                    }
                                </Button>
                            </CardImgOverlay>
                            <CardBody>
                                <CardTitle>{mercenary.name}</CardTitle>
                                <CardText>attack speed is {mercenary.attackSpeed}</CardText>
                                <CardText>attack damage is {mercenary.attackDamage}</CardText>
                            </CardBody>
                        </Card>
                   
                </div>
            );
                                }

    }

  

    

    const MercDetail = (props) => {
        console.log("props are",props)
        if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if (props.mercenary != null)        
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/mercShop'>Merc Shop</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{props.mercenary.name}</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.mercenary.name}</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <RenderItem deleteEquippedCurrentShop={props.deleteEquippedCurrentShop}roster = {props.roster} mercenary={props.mercenary} 
                                equipped={props.equipped} postMercBench = {props.postMercBench} postMercEquipped={props.postMercEquipped} gold={props.gold} 
                                updateGold={props.updateGold}/>
                        
                    </div>
                </div>
            );
        else
            return(
                <div></div>
            );
    }

export default MercDetail;