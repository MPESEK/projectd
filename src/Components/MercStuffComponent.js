
import React from 'react';

import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { Card, CardImg, CardImgOverlay,
    CardTitle } from 'reactstrap';

import { baseUrl } from '../shared/baseUrl';




const MercStuff = (props) => {
   
  
        

        if (props.props.equipped.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }

        var myMercs = props.props.mercenarys.mercenarys
        
        
    
        
        
        var randomArray = Array.from({length: 3}, () => Math.floor(Math.random() * 30+1));
        return(
            
            randomArray.map(function(name,index){
                var mercenary = myMercs[name]
                return <div>
                        <Card>
                            <Link to={`/MercShop/${mercenary._id}`} >
                            <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                                <CardImgOverlay>
                                    <CardTitle>{mercenary.name}</CardTitle>
                                </CardImgOverlay>
                            </Link>
                        </Card>
                    
                        </div>

                
            })
            
        )
    
    
    
    

        
    
        
    }
        
        
        
    






export default MercStuff;