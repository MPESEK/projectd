import React, { Component } from 'react';
import {  Breadcrumb, BreadcrumbItem,  Button} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';

import { updateGold } from '../redux/ActionCreators';

    function RenderResults({number,props,equipped,changeGold}) {

        /* Go through equipped to calculate the stats and synergies of user's team*/
        var mages = 0;
        var druids = 0;
        var warriors = 0;
        var beasts = 0;
        var undead = 0;
        
        if (props.equipped.isLoading) {
            
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        var i = 0
        var beasts = 0
        var undead = 0
        var beastsg3 = 3
        var demon = 0
        var fae = 0
        var celestial = 0
        var nightmare = 0

        var demong3 = 3
        var undeadg3 = 3
        var celestialg3 = 3
        var faeg3 = 3
        var changeling = 0
        var spark = 0
        var ranged = 0 
        var aberration = 0
        var frost = 0
        var inferno = 0
        var nightmare = 0
        var poison = 0
        var warrior = 0
        var ethereal = 0
        var priest = 0


        while (i < equipped.equipped.mercenarys.length){
            const roster = equipped.equipped.mercenarys[i]
            
            const rosterStr = roster.name.split("_")
            const origin_2 = rosterStr[2]
            var origin_3 = ""
            
            if(rosterStr.length > 2){
                origin_3 = rosterStr[3]
            }
            
            if (roster.origin == 'beast'){
                beasts++
            }
            if(roster.origin == 'undead'){
                undead++;
            }
            if(roster.origin == 'fae'){
                fae++;
            }
            if(roster.origin == 'celestial'){
                celestial++;
            }
            if(roster.origin == 'demon'){
                demon++;
            }
            if((origin_2 || origin_3) == 'changeling'){
                changeling++;
            }
            if(origin_2 == 'spark'){
                spark++;
            }
            if(origin_2 == 'ranged'){
                ranged++;
            }
            if(origin_2 == 'aberration'){
                aberration++;
            }
            if(origin_2 == 'frost'){
                frost++;
            }
            if(origin_2 == 'inferno'){
                inferno++;
            }
            if(origin_2 == 'nightmare'){
                nightmare++;
            }
            if(origin_2 == 'poison'){
                poison++;
            }
            if(origin_2 == 'warrior'){
                warrior++;
            }
            if(origin_2 == 'ethereal'){
                ethereal++;
            }
            if(origin_2 == 'priest'){
                priest++;
            }
            i++
        }
        if(number == 1){ // this is quest one simple check
            if( beasts >= 3 || demon >=3 || celestial >= 3 || fae >= 3 || undead >= 3 ){
            
                return(
                    
                    <div  className="col-12 col-md-5 m-1">
                        
                        <p>You have succeeded you win and get gold</p>
                        <Button /*href={`/home`}*/ onClick={() => changeGold(100)}>Refresh Gold</Button>
                           
                    </div>
                );
            }
            else return(
                <div  className="col-12 col-md-5 m-1">
                        
                        <p>You have NOT succeeded but take your gold</p>
                        <Button /*href={`/home`}*/ onClick={() => changeGold(70)}>Refresh Gold</Button>
                           
                    </div>
            );
    
            
        }
        
    }

    class QuestDetail extends Component {
        constructor(props) {
            super(props);
            
          }
        
        render () {
        if (this.props.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (this.props.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{this.props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else      
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/quest'>Quest</Link></BreadcrumbItem>
                            <BreadcrumbItem active></BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>quest name</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <RenderResults number = {this.props.number}equipped = {this.props.equipped} props = {this.props} roster = {this.props.roster} 
                                    mercenary={this.props.mercenary} postMercBench = {this.props.postMercBench} postMercEquipped={this.props.postMercEquipped} 
                                    gold={this.props.gold} changeGold={this.props.changeGold}/>
                        
                    </div>
                </div>
            );
        
    }
}

export default QuestDetail;