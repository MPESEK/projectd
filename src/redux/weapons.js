import { WEAPONS } from '../shared/weapons';
import * as ActionTypes from './ActionTypes';
export const Weapons = (state = { isLoading: true,
    errMess: null,
    weapons:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_WEAPONS:
            return {...state, isLoading: false, errMess: null, weapons: action.payload};

        case ActionTypes.WEAPONS_LOADING:
            return {...state, isLoading: true, errMess: null, weapons: []}

        case ActionTypes.WEAPONS_FAILED:
            return {...state, isLoading: false, errMess: action.payload, weapons: []};

        default:
            return state;
    }
};