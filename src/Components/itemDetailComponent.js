import React from 'react';
import { Card, CardImg, CardImgOverlay, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem, 
    Button, } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';


    function RenderItem({weapon, equipped, postEquipped,gold,updateGold}) {
            
            const wrapperFunction = (id,gold) => {

                
                updateGold(gold-50);
               
                postEquipped(id);
            }
            if (gold.gold < 0){
                return <div>Go do some quests</div>
            }
            else{
            return(
                <div className="col-12 col-md-5 m-1">
                    <p>Hello</p>
                        <Card>
                            <CardImg top src={baseUrl + weapon.image} alt={weapon.name} />
                            <CardImgOverlay>
                                <Button outline color="primary" onClick={() => equipped ? console.log('Already equipped') :wrapperFunction(weapon._id,gold)}>
                                    {equipped ?
                                        <span className="fa fa-heart"></span>
                                        : 
                                        <span className="fa fa-heart-o"></span>
                                    }
                                </Button>
                            </CardImgOverlay>
                            <CardBody>
                                <CardTitle>{weapon.name}</CardTitle>
                                <CardText>attack speed is {weapon.attackSpeed}</CardText>
                                <CardText>attack damage is {weapon.attackDamage}</CardText>
                            </CardBody>
                        </Card>
                   
                </div>
            );
                                }

    }

  

    

    const ItemDetail = (props) => {
        if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if (props.weapon != null)        
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/itemShop'>Item Shop</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{props.weapon.name}</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.weapon.name}</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <RenderItem weapon={props.weapon} equipped={props.equipped} postEquipped={props.postEquipped} gold={props.gold} updateGold={props.updateGold}/>
                        
                    </div>
                </div>
            );
        else
            return(
                <div></div>
            );
    }

export default ItemDetail;