import React from 'react';
import { Loading } from './LoadingComponent';

import { Card, CardImg,  Breadcrumb,Button,CardSubtitle} from 'reactstrap';
import { Link } from 'react-router-dom';
import { baseUrl } from '../shared/baseUrl';

    function RenderRoster ({mercenary,deleteEquippedMerc,postMercBench, onClick}) {
        const moveToBench = (mercenaryId) => {
                deleteEquippedMerc(mercenaryId);
                postMercBench(mercenaryId);


        }
        var str = mercenary.name.split("_")
        str = "\n"+str[0]+"\n " +str[1]+"\n" + str[2]
        
        if(mercenary.isLegendary){
            return (
                <div>
                <Card body inverse style={{ backgroundColor: '#DAA520', borderColor: '#333' }}>
                    <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                        
                            <CardSubtitle>{str}</CardSubtitle>
                            
                        
                </Card>
                <Button outline color="danger" onClick={() => moveToBench(mercenary._id)}>
                    <span className="fa fa-times"></span>
                </Button>
                </div>
            );

        }
        else {
            return (
                <div>
                <Card body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                    <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                        
                            <CardSubtitle>{str}</CardSubtitle>
                            
                        
                </Card>
                <Button outline color="danger" onClick={() => moveToBench(mercenary._id)}>
                    <span className="fa fa-times"></span>
                </Button>
                </div>
            );

        }
        
    }
    function RenderBench ({mercenary,deleteEquippedBench,postMercBench,postMercEquipped, onClick}) {
        const moveToBench = (mercenaryId) => {
                deleteEquippedBench(mercenaryId);
        }
        const moveToRoster = (mercenaryId) => {
            postMercEquipped(mercenaryId);
            deleteEquippedBench(mercenaryId);
    }
        var str = mercenary.name.split("_")
        str = "\n"+str[0]+"\n " +str[1]+"\n" + str[2]
        

        if(mercenary.isLegendary){
        return (
            <div>
            <Card body inverse style={{ backgroundColor: '#DAA520', borderColor: '#333' }}>
                <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                    
                <CardSubtitle>{str}</CardSubtitle>
                    
            </Card>
            <Button outline color="danger" onClick={() => moveToBench(mercenary._id)}>
                <span className="fa fa-times"></span>
            </Button>
            <Button outline color="success" onClick={() => moveToRoster(mercenary._id)}>
                <span className="fa fa-times"></span>
            </Button>
            </div>
        );

        }
        else{
            return (
                <div>
                <Card body inverse style={{ backgroundColor: '#111', borderColor: '#333' }}>
                    <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                        
                    <CardSubtitle>{str}</CardSubtitle>
                    
                </Card>
                <Button outline color="danger" onClick={() => moveToBench(mercenary._id)}>
                    <span className="fa fa-times"></span>
                </Button>
                <Button outline color="success" onClick={() => moveToRoster(mercenary._id)}>
                    <span className="fa fa-times"></span>
                </Button>
                </div>
            );

        }
        
    }
    

    const Roster = (props) => {
        if (props.equipped.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.equipped.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.equipped.errMess}</h4>
                    </div>
                </div>
            );
        }
        
      
        const mercs = props.equipped.equipped.mercenarys.map((mercenary) => {
            return (
                <div key={mercenary._id} className="col-12 col-md-2 m-1">
                    <RenderRoster mercenary={mercenary} deleteEquippedMerc={props.deleteEquippedMerc} postMercBench = {props.postMercBench} />
                </div>
            );
        });
        const bench = props.equipped.equipped.bench.map((mercenary) => {
            return (
                <div key={mercenary._id} className="col-12 col-md-2 m-1">
                    <RenderBench mercenary={mercenary} deleteEquippedMerc={props.deleteEquippedMerc} 
                                deleteEquippedBench = {props.deleteEquippedBench} 
                                postMercBench= {props.postMercBench}
                                postMercEquipped = {props.postMercEquipped}/>
                </div>
            );
        });
    

       
    

        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <Breadcrumb><Link to="/home">Home</Link></Breadcrumb>
                        <Breadcrumb active>Roster</Breadcrumb>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Roster</h3><p>If you have more than 5 on your roster, 5 will be selected randomly upon entering a quest. Use red button to move to bench</p>
                        <hr />
                    </div>                
                </div>
                <div className="row">
                    {mercs}
                </div>
                <h3>Bench</h3><p>Use green button to move to roster, red button will remove the mercenary entirely</p>
                <div className="row">
                    {bench}
                </div>
            </div>
        );
    }

export default Roster;