
import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { Card, CardImg,
    CardTitle, Breadcrumb,BreadcrumbItem } from 'reactstrap';

import { baseUrl } from '../shared/baseUrl';




function RenderMercs(props){
    return (
        props.props.equipped.equipped.currentShop.map(function(name,index){
            var mercenary = name
            var str = ""
            /*console.log("index is", index)*/
            if(mercenary.name == null){
                str = ["null","null","null"]
            }
            else{
                str = mercenary.name.split("_")
            }
            
            if(mercenary.isLegendary){
                return <div key ={index}>
                    <Card body inverse style={{ backgroundColor: '#DAA520', borderColor: '#333' }}>
                    <Link to={`/MercShop/${mercenary._id}/${mercenary.legendaryVersionId}`} >
                        <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                            
                                <CardTitle>{str[0]+" " +str[1]+" " + str[2]}</CardTitle>
                            
                        </Link>
                    </Card>
                
                 </div>
            }
            else{
                return <div key = {index}>
                <Card body inverse style={{ backgroundColor: '#111', borderColor: '#333' }}>
                    <Link to={`/MercShop/${mercenary._id}/${mercenary.legendaryVersionId}`} >
                    <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                        
                            <CardTitle>{str[0]+" " +str[1]+" " + str[2]}</CardTitle>
                        
                    </Link>
                </Card>
            
             </div>

            }
        }))
                

            
            

    
    

}
const MercStuff2 = (props) => {

        if (props.props.equipped.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        if (props.props.gold < 10){
            return <div>no gold do quests</div>
        }
     
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Merc Shop</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Merc Shop</h3><p>Click on a mercenary to go to purchase page</p>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <RenderMercs props={props.props}></RenderMercs>
                </div>
                
                    
                
                

            </div>
        );
                
        }
        
        
        

        
    
        
    
        
        
        
    






export default MercStuff2;