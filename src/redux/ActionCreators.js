import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';



export const fetchWeapons = () => (dispatch) => {
    dispatch(weaponsLoading(true));

    return fetch(baseUrl + 'weapons')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json())
        .then(dishes => dispatch(addWeapons(dishes)))
        .catch(error => dispatch(weaponsFailed(error.message)));
}
export const weaponsLoading = () => ({
    type: ActionTypes.WEAPONS_LOADING
});

export const weaponsFailed = (errmess) => ({
    type: ActionTypes.WEAPONS_FAILED,
    payload: errmess
});


export const addWeapons = (weapons) => ({
    type: ActionTypes.ADD_WEAPONS,
    payload: weapons
});

export const updateGold = (amount) => ({
    type: ActionTypes.UPDATE_GOLD,
    payload: amount

});





export const requestLogin = (creds) => {
    return {
        type: ActionTypes.LOGIN_REQUEST,
        creds
    }
}
  
export const receiveLogin = (response) => {
    return {
        type: ActionTypes.LOGIN_SUCCESS,
        token: response.token
    }
}
  
export const loginError = (message) => {
    return {
        type: ActionTypes.LOGIN_FAILURE,
        message
    }
}

export const loginUser = (creds) => (dispatch) => {
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds))

    return fetch(baseUrl + 'users/login', {
        method: 'POST',
        headers: { 
            'Content-Type':'application/json' 
        },
        body: JSON.stringify(creds)
    })
    .then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        },
        error => {
            throw error;
        })
    .then(response => response.json())
    .then(response => {
        if (response.success) {
            // If login was successful, set the token in local storage
            localStorage.setItem('token', response.token);
            localStorage.setItem('creds', JSON.stringify(creds));
            // Dispatch the success action
            dispatch(receiveLogin(response));
        }
        else {
            var error = new Error('Error ' + response.status);
            error.response = response;
            throw error;
        }
    })
    .catch(error => dispatch(loginError(error.message)))
    
};
export function snl(creds){
  return(dispatch, getState) =>{
    return dispatch(signupUser(creds).then(() => {
      return dispatch(loginUser(creds))

    }))
    
  }
    


}

export const signupUser = (creds) => (dispatch) => {
  // We dispatch requestLogin to kickoff the call to the API
  

  return fetch(baseUrl + 'users/signup', {
      method: 'POST',
      headers: { 
          'Content-Type':'application/json' 
      },
      body: JSON.stringify(creds)
  })
  .then(response => {
      if (response.ok) {
          return response;
      } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
      }
      },
      error => {
          throw error;
      })
  .then(response => response.json())
  .then(response => {
      if (response.success) {
          /* If login was successful, set the token in local storage
          localStorage.setItem('token', response.token);
          localStorage.setItem('creds', JSON.stringify(creds));
          // Dispatch the success action*/
          return response;
          
      }
      else {
          var error = new Error('Error ' + response.status);
          error.response = response;
          throw error;
      }
  })
  .then(response => response.json())
  .then(dispatch(loginError("hi")))
  .catch(error => dispatch(loginError(error.message)))
  
};

export const requestLogout = () => {
    return {
      type: ActionTypes.LOGOUT_REQUEST
    }
}
  
export const receiveLogout = () => {
    return {
      type: ActionTypes.LOGOUT_SUCCESS
    }
}

// Logs the user out
export const logoutUser = () => (dispatch) => {
    dispatch(requestLogout())
    localStorage.removeItem('token');
    localStorage.removeItem('creds');
    
    dispatch(receiveLogout())
}



export const postEquipped = () => (dispatch) => {
    const bearer = 'Bearer ' + localStorage.getItem('token');
    console.log("bearer is", bearer)
    return fetch(baseUrl + 'equipped', {
        method: "PUT",
        body: null,
        headers: {
          'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('item Added', equipped); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
}

export const deleteEquipped = (weaponId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/' + weaponId, {
        method: "DELETE",
        headers: {
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('Favorite Deleted', equipped); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
};

export const deleteEquippedMerc = (mercenaryId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/mercenary/' + mercenaryId, {
        method: "DELETE",
        headers: {
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('Merc Deleted', equipped); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
};
export const deleteEquippedBench = (mercenaryId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/bench/' + mercenaryId, {
        method: "DELETE",
        headers: {
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('Merc Deleted', equipped); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
};
export const fetchEquipped = () => (dispatch) => {
    dispatch(equippedLoading(true));

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped', {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(equipped => dispatch(addEquipped(equipped)))
    .catch(error => dispatch(equippedFailed(error.message)));
}

export const equippedLoading = () => ({
    type: ActionTypes.EQUIPPED_LOADING
});

export const equippedFailed = (errmess) => ({
    type: ActionTypes.EQUIPPED_FAILED,
    payload: errmess
});

export const addEquipped = (equipped) => ({
    type: ActionTypes.ADD_EQUIPPED,
    payload: equipped
});

export const fetchMercenarys = () => (dispatch) => {
    dispatch(mercenarysLoading(true));

    return fetch(baseUrl + 'mercenarys')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json())
        .then(mercenarys => dispatch(addMercenarys(mercenarys)))
        .catch(error => dispatch(mercenarysFailed(error.message)));
}
export const mercenarysLoading = () => ({
    type: ActionTypes.MERCENARYS_LOADING
});

export const mercenarysFailed = (errmess) => ({
    type: ActionTypes.MERCENARYS_FAILED,
    payload: errmess
});


export const addMercenarys = (mercenarys) => ({
    type: ActionTypes.ADD_MERCENARYS,
    payload: mercenarys
});

export const postMercEquipped = (mercenaryId) => (dispatch) => {
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/mercenary/' + mercenaryId, {
        method: "POST",
        body: JSON.stringify({"_id": mercenaryId}),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('merc Added', equipped); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
}

export const postMercBench = (mercenaryId) => (dispatch) => {
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/bench/' + mercenaryId, {
        method: "POST",
        body: JSON.stringify({"_id": mercenaryId}),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('merc Added', equipped); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
}
export const postMercCurrentShop = (mercenaryId) => (dispatch) => {
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/currentShop/' + mercenaryId, {
        method: "POST",
        body: JSON.stringify({"_id": mercenaryId}),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('merc Added', mercenaryId); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
}

export const deleteEquippedCurrentShop = (mercenaryId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/currentShop/' + mercenaryId, {
        method: "DELETE",
        headers: {
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(equipped => { console.log('Merc Deleted', equipped); dispatch(addEquipped(equipped)); })
    .catch(error => dispatch(equippedFailed(error.message)));
};

export const toggleQuestCompleted = () => (dispatch) => {
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'equipped/toggleQuest', {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .catch(error => dispatch(equippedFailed(error.message)));
}


export const deleteAllEquippedCurrentShop = (mercenaryId) => (dispatch) => {

  const bearer = 'Bearer ' + localStorage.getItem('token');

  return fetch(baseUrl + 'equipped/CS/', {
      method: "DELETE",
      headers: {
        'Authorization': bearer
      },
      credentials: "same-origin"
  })
  .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
          throw error;
    })
  .then(response => response.json())
  .then(equipped => { console.log('Merc Deleted', equipped); dispatch(addEquipped(equipped)); })
  .catch(error => dispatch(equippedFailed(error.message)));
};

export const changeGold = (goldAmount) => (dispatch) => {
  const bearer = 'Bearer ' + localStorage.getItem('token');

  return fetch(baseUrl + 'equipped/changeGold/' + goldAmount, {
      method: "PUT",
      body: JSON.stringify({"_id": goldAmount}),
      headers: {
        "Content-Type": "application/json",
        'Authorization': bearer
      },
      credentials: "same-origin"
  })
  .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
          throw error;
    })
  .then(response => response.json())
  .then(equipped => { console.log('gold changed', equipped); dispatch(addEquipped(equipped)); })
  .catch(error => dispatch(equippedFailed(error.message)));
}