
import {Gold} from './gold';
import {Auth} from './auth';
import {Equipped} from './equipped';
import {Mercenarys} from './mercenarys';

import {createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';


export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            gold: Gold,
            auth: Auth,
            equipped: Equipped,
            mercenarys: Mercenarys
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
}