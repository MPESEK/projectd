import React from 'react';
import { Loading } from './LoadingComponent';

import { Card, CardImg, CardImgOverlay,CardBody,
    CardTitle, Breadcrumb,CardSubtitle, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { baseUrl } from '../shared/baseUrl';

    function RenderRoster ({mercenary,deleteEquippedMerc,postMercBench, onClick}) {
        const moveToBench = (mercenaryId) => {
                deleteEquippedMerc(mercenaryId);
                postMercBench(mercenaryId);


        }
        
        return (
            <div>
            <Card>
                
                <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                    <CardImgOverlay>
                        <CardTitle>{mercenary.name}</CardTitle>
                    </CardImgOverlay>
               
            </Card>
            
            <Button outline color="danger" onClick={() => moveToBench(mercenary._id)}>
                <span className="fa fa-times"></span>
            </Button>
            </div>
        );
    }
    

    const Quest = (props) => {

        if (props.equipped.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.equipped.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.equipped.errMess}</h4>
                    </div>
                </div>
            );
        }
        else
       
    

      
    

        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <Breadcrumb><Link to="/home">Home</Link></Breadcrumb>
                        <Breadcrumb active>Roster</Breadcrumb>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Quest</h3>
                        <hr />
                    </div>
                    <Card>
                       <CardImg top width="50%" src={baseUrl + "images/elephant_tusks.jpeg"}/>
                        <CardBody>
                            <CardTitle>Quest 1</CardTitle>
                            <CardSubtitle>The mighty boar</CardSubtitle>
                            <Button href={`/questDetail/1`}>Embark</Button>
                        </CardBody>
                            
                    </Card>
                    <Card>
                    <CardImg top width="50%" src={baseUrl + "images/ship_embarking.jpeg"}/>
                        <CardBody>
                            <CardTitle>Quest 2</CardTitle>
                            <CardSubtitle>Ship assail</CardSubtitle>
                            <Button>To Be Added</Button>
                        </CardBody>
                            
                    </Card>
                    <Card>
                    <CardImg top width="50%" src={baseUrl + "images/wizard_dragon.jpeg"}/>
                        <CardBody>
                            <CardTitle>Quest 3</CardTitle>
                            <CardSubtitle>Dragon Mage</CardSubtitle>
                            <Button>To Be Added</Button>
                        </CardBody>
                            
                    </Card>          
                </div>
                
            </div>
        );
    }

export default Quest;