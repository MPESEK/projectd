import React from 'react';
import { Loading } from './LoadingComponent';

import { Card, CardImg, CardImgOverlay,
    CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { baseUrl } from '../shared/baseUrl';

    function RenderItemShopItem ({weapon, onClick}) {
        return (
            <Card>
                <Link to={`/itemShop/${weapon._id}`} >
                <CardImg top width="50%" src={baseUrl + weapon.image} alt={weapon.name} />
                    <CardImgOverlay>
                        <CardTitle>{weapon.name}</CardTitle>
                    </CardImgOverlay>
                </Link>
            </Card>
        );
    }

    const ItemShop = (props) => {
        
        const ItemShop = props.weapons.weapons.map((weapon) => {
            if (props.weapons.isLoading) {
                return(
                    <div className="container">
                        <div className="row">            
                            <Loading />
                        </div>
                    </div>
                );
            }
            else if (props.weapons.errMess) {
                return(
                    <div className="container">
                        <div className="row"> 
                            <div className="col-12">
                                <h4>{props.weapons.errMess}</h4>
                            </div>
                        </div>
                    </div>
                );
            }
            else
            return (
                <div className="col-12 col-md-5 m-1"  key={weapon.id}>
                    <RenderItemShopItem weapon={weapon} onClick={props.onClick} />
                </div>
            );
        });

        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>ItemShop</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>ItemShop</h3>
                        <hr />
                    </div>                
                </div>
                <div className="row">
                    {ItemShop}
                </div>
            </div>
        );
    }

export default ItemShop;