
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col,CardImg,CardImgOverlay } from 'reactstrap';
import classnames from 'classnames';
function RenderMercenary({ mercenary, onClick }) {
    return(
        <Card>
            
                <CardImg width="100%" src={baseUrl + mercenary.image} alt={mercenary.name} />
                <CardImgOverlay>
                    <CardTitle>{mercenary.name}</CardTitle>
                </CardImgOverlay>
            
        </Card>
    );
}




const Stats = (props) => {
    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
        if(activeTab !== tab) setActiveTab(tab);
    }



    
    if (props.equipped.isLoading) {
        return(
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.equipped.errMess) {
        return(
            <div className="container">
                <div className="row">
                    <h4>{props.equipped.errMess}</h4>
                </div>
            </div>
        );
    }
    else
    console.log("made c", props.equipped.equipped.mercenarys)
    const mercs = props.equipped.equipped.mercenarys.map((mercenary) => {
        return (
            <div key={mercenary._id} className="col-12 col-md-5 m-1">
                <RenderMercenary mercenary={mercenary} />
            </div>
        );
    });
    return(
        
        <div>
            <h4>Character Stats</h4>
        <Nav tabs>
            <NavItem>
            <NavLink
                className={classnames({ active: activeTab === '1' })}
                onClick={() => { toggle('1'); }}
            >
                Tab1
            </NavLink>
            </NavItem>
            <NavItem>
            <NavLink
                className={classnames({ active: activeTab === '2' })}
                onClick={() => { toggle('2'); }}
            >
                Moar Tabs
            </NavLink>
            </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
            <TabPane tabId="1">
            <Row>
                <Col sm="12">
                <div className="row">
                        {mercs}
                    </div>
                </Col>
            </Row>
            </TabPane>
            <TabPane tabId="2">
            <Row>
                <Col sm="6">
                <Card body>
                    <CardTitle>Special Title Treatment</CardTitle>
                    <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                    <Button>Go somewhere</Button>
                </Card>
                </Col>
                <Col sm="6">
                <Card body>
                    <CardTitle>Special Title Treatment</CardTitle>
                    <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                    <Button>Go somewhere</Button>
                </Card>
                </Col>
            </Row>
            </TabPane>
        </TabContent>
    </div>
    );
}

export default Stats;    