import React from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem, Button} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';


    function RenderItem({roster,mercenary, equipped, postMercEquipped,postMercBench,gold,changeGold,deleteEquippedCurrentShop,deleteEquippedBench,deleteEquippedMerc}) {
           

            
            const wrapperFunction = (equipped,id,legId,gold) => {

                
                
                var i = 0
                var rosterCount = 0
                var benchCount = 0
                var currentShopCount = 0
                while(i < equipped.equipped.mercenarys.length){
                    if (equipped.equipped.mercenarys[i]._id == id){
                        rosterCount++
                    }
                    i++

                }
                var j = 0
                while(j < equipped.equipped.bench.length){
                    if (equipped.equipped.bench[j]._id == id){
                        benchCount++
                    }
                    j++

                    
                }
                var k = 0
                while(k < equipped.equipped.currentShop.length){
                    if (equipped.equipped.currentShop[k]._id == id){
                        currentShopCount++
                    }
                    k++
                    
                }
                if(equipped.equipped.bench.length >9){
                    alert("You cannot have more than 9 on the bench, create space before you buy your next merc")
                    return
                        
                }
                // this block combines into a legendary
                if(rosterCount == 0 && benchCount == 0 && currentShopCount == 2){
                    if(equipped.equipped.bench <9){
                        if(gold == 10){ // We want the game to feel good, if they only have 10 gold but a double is in shop, let them buy both
                            changeGold(gold - 10)
                            postMercBench(id)
                            postMercBench(id)
                            deleteEquippedCurrentShop(id)
                            return

                        }
                        changeGold(gold - 20)
                        postMercBench(id)
                        postMercBench(id)
                        deleteEquippedCurrentShop(id)
                        return
                    }
                }
                if((rosterCount+benchCount+currentShopCount) >= 3){
                    if(currentShopCount == 20){
                        changeGold(gold - 20)
                    }
                    if(roster.length > 4 && rosterCount == 0){
                        changeGold(gold-10)
                        postMercBench(legId)
                        deleteEquippedCurrentShop(id)
                        deleteEquippedMerc(id)
                        deleteEquippedBench(id)
                        return

                    }
                    else{
                        changeGold(gold-10)
                        postMercEquipped(legId)
                        deleteEquippedCurrentShop(id)
                        deleteEquippedMerc(id)
                        deleteEquippedBench(id)
                        return
                        
                    }
                    
                }
                if(currentShopCount == 2){
                    if(gold <20){
                        changeGold(gold - 10)
                        if(roster.length < 5){
                            postMercEquipped(id)
                            return
                        }
                        else{
                            postMercBench(id)
                            return
                        }
                    }
                    if(roster.length <4){
                        changeGold(gold - 20)
                        postMercEquipped(id)
                        postMercEquipped(id)
                        deleteEquippedCurrentShop(id)
                        return
                    }
                    else if(roster.length == 4){
                        changeGold(gold - 20)
                        postMercEquipped(id)
                        postMercBench(id)
                        deleteEquippedCurrentShop(id)
                        return
                    }
                    else{
                        changeGold(gold - 20)
                        postMercBench(id)
                        postMercBench(id)
                        deleteEquippedCurrentShop(id)
                        return
                    }
                }
                if(gold <10){
                    changeGold(500)
                    return
                }
                
                

                if (roster.length < 5) {
                    changeGold(gold - 10)
                    postMercEquipped(id);
                    deleteEquippedCurrentShop(id);
                }
                else{
                    changeGold(gold - 10)
                    postMercBench(id);
                    deleteEquippedCurrentShop(id);
                }
                
            } // end of wrapper function
            if (gold.gold < -5000){
                return <div>Go do some quests</div>
            }
            else{
            return(
                <div style={{display:"flex", flexDirection: "row"}} >
                <div style={{display:"flex", flexDirection: "row"}} className="col-12 col-md-6 m-1">
                    
                    <Card body inverse style={{ backgroundColor: '#111', borderColor: '#333' }}>
                            <CardImg width = "100%" top src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                            
                                <Button color="primary" onClick={() => wrapperFunction(equipped,mercenary._id,mercenary.legendaryVersionId,gold)}>
                                    {equipped ?
                                        <span className="fa fa-credit-card fa-lg"></span>
                                        : 
                                        <span className="fa fa-credit-card fa-lg"></span>
                                    }
                                </Button>
                            
                            <CardBody>
                                <CardTitle>{mercenary.name}</CardTitle>
                                <CardText>attack speed is {mercenary.attackSpeed}</CardText>
                                <CardText>attack damage is {mercenary.attackDamage}</CardText>
                            </CardBody>
                        </Card>  
                </div>

                <div style={{display:"flex", flexDirection: "row"}} className="col-12 col-md-6 m-1">
                    
                    <Card body inverse style={{ backgroundColor: '#DAA520', borderColor: '#333' }}>
                            <CardImg width = "100%" top src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                            
                                
                            
                            <CardBody>
                                <CardTitle style={{color:"black"}}>Purchase 3 to combine into {"\n"} {mercenary.name + " legendary"}</CardTitle>
                                <CardText>attack speed is {mercenary.attackSpeed * 2}</CardText>
                                <CardText>attack damage is {mercenary.attackDamage * 2}</CardText>
                            </CardBody>
                        </Card>  
                </div>
                </div>
                
            );
                                }

    }

  

    

    const MercDetail = (props) => {
        console.log("props are",props)
        if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if (props.mercenary != null)
            if(props.gold < 10){
                return <div>go do some quests</div>

            }
            else  
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/mercShop'>Merc Shop</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{props.mercenary.name}</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.mercenary.name}</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <RenderItem deleteEquippedCurrentShop={props.deleteEquippedCurrentShop}roster = {props.roster} mercenary={props.mercenary} 
                                deleteEquippedBench={props.deleteEquippedBench} deleteEquippedMerc={props.deleteEquippedMerc}
                                equipped={props.equipped} postMercBench = {props.postMercBench} postMercEquipped={props.postMercEquipped} gold={props.gold} 
                                changeGold={props.changeGold}/>
                        
                    </div>
                </div>
            );
        
    }

export default MercDetail;