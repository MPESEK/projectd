var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({
    class: {
      type: String,
        default: ''
    },
    hitpoints: {
      type: String,
        default: ''
    },
    strength:   {
        type: Int8Array,
        default: false
    },
    dexterity:   {
        type: Int8Array,
        default: false
    },
});

User.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', User);