import React from 'react';
import { Loading } from './LoadingComponent';

import { Card, CardImg, CardImgOverlay,
    CardTitle, Breadcrumb } from 'reactstrap';
import { Link } from 'react-router-dom';
import { baseUrl } from '../shared/baseUrl';

    function RenderMercShopMerc ({mercenary, onClick}) {
        return (
            <Card>
                <Link to={`/MercShop/${mercenary._id}`} >
                <CardImg top width="50%" src={baseUrl + "images/"+ mercenary.image} alt={mercenary.name} />
                    <CardImgOverlay>
                        <CardTitle>{mercenary.name}</CardTitle>
                    </CardImgOverlay>
                </Link>
            </Card>
        );
    }

    const MercShop = (props) => {
     
    
        const MercShop = (props)=> {
            if (props.mercenarys.isLoading) {
                return(
                    <div className="container">
                        <div className="row">            
                            <Loading />
                        </div>
                    </div>
                );
            }
            else if (props.mercenarys.errMess) {
                return(
                    <div className="container">
                        <div className="row"> 
                            <div className="col-12">
                                <h4>{props.mercenarys.errMess}</h4>
                            </div>
                        </div>
                    </div>
                );
            }
            else
            {
            var index = 0
            
            while( index < props.mercenarys.length){
                if (index <5)
                {
                        return (
                            <div className="col-12 col-md-2 m-1"  key={mercenary.id}>
                                <RenderMercShopMerc mercenary={mercenary} onClick={props.onClick} />
                            </div>
                        );
                }
                index++;

            }
        }
        }

        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <Breadcrumb><Link to="/home">Home</Link></Breadcrumb>
                        <Breadcrumb active>MercShop</Breadcrumb>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>MercShop</h3>
                        <hr />
                    </div>                
                </div>
                <div className="row">
                    {MercShop}
                </div>
            </div>
        );
    }

export default MercShop;